"use strict";

var direction = 0;
var prefetch;
var timeNow = Date.now();
var longitude;
var latitude;
var longitudeMin;
var latitudeMin;
var longitudeMax;
var latitudeMax;
var mapMode = false;
var ajout = true;
var layers1;
liste = [];
var added = false;
Geofinal = [];
var layerToRemove = [];
var assetLayerGroup = new L.LayerGroup();
var mymap = L.map('mapid').setView([45.180764, 5.726884], 12.5);

window.onload = function () {
  $('#miniIco').hide();
  $('.modalBus').hide();
  $('.modal').hide();
  $('#mapid').hide();
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFqZXN0aWM1MzUyMzAiLCJhIjoiY2tlYmgxejNhMDV6bjJ0bnhtcHY2NW4zMiJ9.7Iwuo697wzheAtx7H5SrHg'
  }).addTo(mymap);
};

$('document').ready(function () {
  $('#miniIco').on('click', '.miniClose', function (e) {
    console.log('ok');
    console.log(e.target.id);
    mymap.removeLayer(layers1);
    traceCarteRemove(e.target.id);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoibWFqZXN0aWM1MzUyMzAiLCJhIjoiY2tlYmgxejNhMDV6bjJ0bnhtcHY2NW4zMiJ9.7Iwuo697wzheAtx7H5SrHg'
    }).addTo(mymap);
    $('#miniIco').empty();
  });

  function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
  }

  var popup = L.popup();

  function onMapClick(e) {
    popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn(mymap);
  }

  mymap.on('click', onMapClick);
  mymap.on('click', onMapClick);
  $('#metro').click(function () {
    console.log('ok');
    $('#miniIco').hide();
    $('#miniIco').empty();
    $('#onglets').show();
    $('.modal').hide();
    $('.modalBus').hide();
    $('#results').empty();
    $('#mapid').hide();
  });
  $('#icoHoraires').click(function () {
    $('#onglets').hide();
    $('.modal').show();
    $('#results').show();
    $('#mapid').hide();
    mapMode = false;
  });
  $('#icoPlan').click(function () {
    $('#miniIco').show();
    $('#results').empty();
    $('#mapid').show();
    $('.modal').show();
    mapMode = true;
  });
  $('.choixTram').click(function () {
    $('#results').empty();
    $('#prevAndNext').empty(); // vider la div Résultat ('.results')

    $('.modalBus').hide();
    $('.select').removeClass('active');
    $('.choixTram').addClass('active');
    recherche('TRAM', 'TRAM');
  });
  $('.choixBus').click(function () {
    $('#results').empty();
    $('#prevAndNext').empty(); // vider la div Résultat ('.results')

    $('.modalBus').show();
    $('.select').removeClass('active');
    $('.choixBus').addClass('active');
  });
  $('.choixRail').click(function () {
    $('#results').empty();
    $('#prevAndNext').empty(); // vider la div Résultat ('.results')

    $('.modalBus').hide();
    $('.select').removeClass('active');
    $('.choixRail').addClass('active'); // recherche('RAIL','SNC')

    console.log('click ok');
    test();
  });
  $('.typeC38').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'C38');
  });
  $('.typeChrono').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'CHRONO');
  });
  $('.typeFlexo').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'FLEXO');
  });
  $('.typeScol').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'SCOL');
  });
  $('.typeNav').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'NAV');
  });
  $('.typeSnc').click(function () {
    $('.modalBus').hide();
    recherche('BUS', 'SNC');
  });
  $('.results').on('click', '.chgtDirection', function () {
    console.log(direction);

    if (direction == 0) {
      direction = 1;
    } else {
      direction = 0;
    }

    horaires(prefetch, timeNow);
  }); // Fonction recherche JSON VIA FETCH

  var recherche = function recherche(mode, type) {
    $('#results').empty();
    $('#prevAndNext').empty(); // vider la div Résultat ('.results')

    fetch('http://data.metromobilite.fr/api/routers/default/index/routes').then(function (Response) {
      return Response.json();
    }).then(function (json) {
      for (i = 0; i < json.length; i++) {
        if (json[i].mode == mode && json[i].type == type) {
          if (mode == 'TRAM') {
            jQuery('<div/>', {
              id: json[i].id,
              "class": 'circle lienLigne',
              "style": "background-color : #".concat(json[i].color),
              "data-color": "#".concat(json[i].color),
              "data-type": "miniIco"
            }).text(json[i].shortName).appendTo('.results');
          } else {
            jQuery('<div/>', {
              id: json[i].id,
              "class": 'carre lienLigne',
              "style": "background-color : #".concat(json[i].color),
              "data-color": "#".concat(json[i].color),
              "data-type": "miniIcobus"
            }).text(json[i].shortName).appendTo('.results');
          }
        }
      }

      $('.lienLigne').click(function (e) {
        // div dynamique => Liens
        var tempId = e.target.id;
        horaires(tempId, timeNow);

        if (mapMode == true && type == 'TRAM') {
          jQuery('<div/>', {
            id: tempId,
            'class': 'miniIco miniClose',
            "style": "background-color : ".concat($(this).data('color'))
          }).text($(this).text()).appendTo('#miniIco');
        } else {
          jQuery('<div/>', {
            id: tempId,
            'class': 'miniIcobus',
            "style": "background-color : ".concat($(this).data('color'))
          }).text($(this).text()).appendTo('#miniIco');
        }
      });
    });
  }; //Fermeture du fetch
  // Fonction recherche LIGNES JSON VIA FETCH  FIN


  var traceCarteRemove = function traceCarteRemove(codeLigne) {
    fetch("http://data.metromobilite.fr/api/lines/json?types=ligne&codes=".concat(codeLigne)).then(function (Response) {
      return Response.json();
    }).then(function (json1) {
      console.log('log de clear : ', json1);
      mymap.removeLayer();
    });
  }; //Fonction affichage ligne sur la carte


  var traceCarte = function traceCarte(codeLigne) {
    fetch("http://data.metromobilite.fr/api/lines/json?types=ligne&codes=".concat(codeLigne)).then(function (Response) {
      return Response.json();
    }).then(function (json) {
      liste = json.features[0].properties.ZONES_ARRET;
      var jsonColor = json.features[0].properties.COULEUR;
      var myStyle = {
        "color": "rgb(".concat(jsonColor, ")")
      };

      if (added == true) {
        added = false;
        L.geoJSON(json, {
          style: myStyle
        }).removeLayer(mymap);
      } else {
        added = true;
        console.log('log de add : ', json);
        L.geoJSON(json, {
          style: myStyle
        }).addTo(mymap);
      }
    });
  }; //Fonction affichage ligne sur la carte FIN
  // Fonction recherche Horaires Json VIA FETCH


  var horaires = function horaires(ligne, temp) {
    return fetch("https://data.metromobilite.fr/api/ficheHoraires/json?route=".concat(ligne, "&time=").concat(temp)).then(function (Response) {
      return Response.json();
    }).then(function (json) {
      console.log('Cest ici : ', json);
      prefetch = ligne;
      console.log('horaires :', json);
      traceCarte(ligne);
      console.log(ajout);
      console.log(json[0].arrets.length);
      layers1 = L.layerGroup();

      for (i = 0; i < json[0].arrets.length; i++) {
        L.marker([json[0].arrets[i].lat, json[0].arrets[i].lon]).bindPopup(json[0].arrets[i].stopName).addTo(layers1);
      }

      mymap.addLayer(layers1); // AFFICHAGE DIRECTION TERMINUS
      // AFFICHAGE DIRECTION TERMINUS

      $('#results').empty();
      $('#prevAndNext').empty();

      if (mapMode == false) {
        /// !!!!!!!!!!!!! SI EN MODE MAP ALORS IGNORE le CALCUL DES HORAIRES !!!!!!!!!!!!!!
        jQuery('<div/>', {
          id: 'precedant',
          "class": 'precedant '
        }).text("<== Horaires Pr\xE9cedent").appendTo('#prevAndNext');
        jQuery('<a/>', {
          id: 'dlHoraires',
          "class": 'dlHoraires ',
          "href": "https://data.metromobilite.fr/api/ficheHoraires/pdf?route=".concat(ligne)
        }).text(" Horaires Complets").appendTo('#prevAndNext');
        jQuery('<div/>', {
          id: 'suivant',
          "class": 'suivant '
        }).text("Horaires Suivant =>").appendTo('#prevAndNext');
        var terminus = json[0].arrets.length - 1;
        jQuery('<ul/>', {
          id: 'liste1',
          "class": 'terminus '
        }).text("Direction => ".concat(json[direction].arrets[terminus].stopName)).appendTo('.results'); // AFFICHAGE DIRECTION TERMINUS FIN
        // AFFICHAGE DIRECTION TERMINUS FIN
        // AFFICHAGE CHANGEMENT DIRECTION
        // AFFICHAGE CHANGEMENT DIRECTION

        jQuery('<span/>', {
          id: 'Direction',
          "class": 'chgtDirection '
        }).appendTo('.results'); // AFFICHAGE CHANGEMENT DIRECTION FIN
        // AFFICHAGE CHANGEMENT DIRECTION FIN
        // AFFICHAGE ARRETS
        // AFFICHAGE ARRETS

        for (i = 0; i < json[direction].arrets.length; i++) {
          jQuery('<li/>', {
            id: i,
            "class": 'arretsLignes'
          }).text(json[direction].arrets[i].stopName).appendTo('#liste1');
        } // AFFICHAGE ARRETS FIN
        // AFFICHAGE ARRETS FIN
        // AFFICHAGE HORAIRES
        // AFFICHAGE HORAIRES


        jQuery('<ul/>', {
          id: 'liste2',
          "class": 'horaires '
        }).text("Horaires : ").appendTo('.results');

        for (i = 0; i < json[direction].arrets.length; i++) {
          var heurePassageA = json[direction].arrets[i].trips[3];
          var date = new Date(0);
          date.setSeconds(heurePassageA);
          var timeString = date.toISOString().substr(11, 8);
          jQuery('<li/>', {
            id: "h" + i,
            "class": 'horairesLigne'
          }).text('Passage à ' + timeString).appendTo('#liste2'); // AFFICHAGE HORAIRES FIN
          // AFFICHAGE HORAIRES FIN

          $('.precedant').click(function () {
            // div dynamique => Liens
            horaires(prefetch, json[direction].prevTime);
            console.log('ok');
          });
          $('.suivant').click(function () {
            // div dynamique => Liens
            horaires(prefetch, json[direction].nextTime);
          });
        } //FIN BOUCLE FOR

      } else {
        $('#results').css('display', 'inline-flex!important');
        $('#results').css('overflow-x', 'scroll');
      }
    });
  }; //FIN DU FETCH

}); // Fonction recherche Horaires Json VIA FETCH END
// $('.box1').css("background-image","url(asset/images/rate1.png)"); 
// $('.box2').css("background-image","url(asset/images/rate2.png)"); 
// $('.box3').css("background-image","url(asset/images/rate3.png)"); 
// $('.myBox').click(function(){
// $('.modal-title').html ($(this).find('.title').text())
// $('.modal-body').html ($(this).find('.lorem').text())
// $('.myBox').removeClass('bg-success') 
// $(this).addClass('bg-success')
// })
$('document').ready(function(){
let timeNow = Date.now()
let  ongletOuvert = true
let layers1 ={}
let horairesMode = false
let mapMode = false
let direction = 0
let mymap = L.map('mapid').setView([45.19709, 5.67222], 13);
let prefetch 
let prevTime
let nextTime
$('#cadreLigne').hide()
$('#fleche').hide()
var popup = L.popup();

let loadMap = () => {
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFqZXN0aWM1MzUyMzAiLCJhIjoiY2tlYmgxejNhMDV6bjJ0bnhtcHY2NW4zMiJ9.7Iwuo697wzheAtx7H5SrHg'
}).addTo(mymap);

}
loadMap()




    $('#btnHoraires').hover(function() { 
        $('.sousAnimBlue').fadeIn(); 
        $('.sousAnimBlue').show(); 
    }, function() { 
        $('.sousAnimBlue').fadeOut(); 
    });

    $('#btnMap').hover(function() { 
        $('.sousAnimRed').fadeIn(); 
        $('.sousAnimRed').show(); 
    }, function() { 
        $('.sousAnimRed').fadeOut(); 
    });

    $('#btnSuivant').hover(function() { 
        $('.sousAnimYellow').fadeIn(); 
        $('.sousAnimYellow').show(); 
    }, function() { 
        $('.sousAnimYellow').fadeOut(); 
    });
    
$('#btnMap').click(function(){
    $('#modalPlan').show()
    $('#mapid').show()
    if (mapMode == true){
        mapMode = false
        ongletOuvert = false
        $('#cadreLigne').animate({opacity :0},1000)
        $('#fleche').animate({opacity :0},1000)
        $('#cadreLigne').hide()
        $('#fleche').hide()
        $('#btnmap').removeClass('active2')
    }else{
        mapMode = true
        ongletOuvert = true
        $('#cadreLigne').show()
        $('#fleche').show()
        $('#cadreLigne').animate({opacity :1},1000)
        $('#fleche').animate({opacity :1},1000)
    
        $('#btnMap').addClass('active2')
    
    }
    



  


})




$('#btnHoraires').click(function (){
    if (horairesMode == true){
    horairesMode = false
    ongletOuvert = false
    $('#cadreLigne').animate({opacity :0},1000)
    $('#fleche').animate({opacity :0},1000)
    $('#cadreLigne').hide()
    $('#fleche').hide()
    $('#btnHoraires').removeClass('active')
}else{
    horairesMode = true
    ongletOuvert = true
    $('#cadreLigne').show()
    $('#fleche').show()
    $('#cadreLigne').animate({opacity :1},1000)
    $('#fleche').animate({opacity :1},1000)

    $('#btnHoraires').addClass('active')

}


})
$('#btnRetourPlan').click(function(){
    $('#btnMap').removeClass('active2')
    $('#modalPlan').hide()
    $('#cadreLigne').hide()
    $('#fleche').hide()
})
$('#fleche').click(function (){

    if (ongletOuvert == true ){
    ongletOuvert = false
    $("#cadreLigne").animate({height: '65px'},1000);
    $("#fleche").animate({top: '65px'},1000);
    $('#mapid').animate({top: '65px'},1000);
    $('#btnRetourPlan').animate({top: '0px'},1000);

}else{
    ongletOuvert = true
    $("#cadreLigne").animate({height: '232px'},1000);
    $("#fleche").animate({top: '232px'},1000);
    $('#mapid').animate({top: '232px'},1000);
    $('#btnRetourPlan').animate({top: '165px'},1000);


}
})


$('#btnSuivantHoraires').click(function(){
    horaires(prefetch, nextTime , direction )   ; 

})

$('#btnPrecedantHoraires').click(function(){
    horaires(prefetch, prevTime , direction)   ; 

})
$.ajax({
    url: "http://data.metromobilite.fr/api/routers/default/index/routes",
    success: function(json) {console.log(json);

        let IcoMaker = (type,start,end,dest) => {
            for (i=start ; i<=end ;i++)
            {
            jQuery('<div/>', 
            {
            id: json[i].id,
            "class": type,
            'draggable':'true',
            'ondragstart': 'drag(event)',
            'ondrop':'drop(event)',
            'ondragover':'allowDrop(event)',
            "style": `background-color : #${json[i].color}; color : #${json[i].textColor};`,
            "data-color" : `#${json[i].color}`,
            "data-type" : "miniIcobus",
            "data-short":`${json[i].shortName}`
            })
            .text(json[i].shortName   ).appendTo(dest);
            }
        }
          
IcoMaker('circle lienLigne',3,7,'#tram')
IcoMaker('carre lienLigne',15,26,'#proximo')
IcoMaker('carre lienLigne',8,14,'#chrono')
IcoMaker('carre lienLigne',27,52,'#flexo')


}});
$('#tram').on('click', '.lienLigne', function(e){   

    if(horairesMode == true){
    $('.lienLigne').removeClass('icoActive')
    $(this).addClass('icoActive')
    $('#modalHoraires').css('display',"flex")
    ongletOuvert = false
    $("#cadreLigne").animate({height: '65px'},1000);
    $("#fleche").animate({top: '65px'},1000);
   
    horaires(e.target.id,timeNow , 0)   ; 
}else{   

    console.log(' say Ok')
    mymap.invalidateSize();

    mymap.eachLayer(function(layer){
            mymap.removeLayer(layer)
            mymap.invalidateSize();

    })

    horaires(e.target.id,timeNow , 0)   ;

    mymap.invalidateSize();

}
});



$('#btnRetourHoraires').click(function(){
ongletOuvert = true
$("#cadreLigne").hide()
$("#fleche").hide()
$('#modalHoraires').hide()
$('#btnHoraires').removeClass('active')
})

$('#direction').click(function(){
    if (direction == 0){direction = 1}
    else {direction = 0}
    horaires(prefetch,timeNow,direction)
})


let horaires = (ligne,temp,direction) =>
$.ajax({
    url: `https://data.metromobilite.fr/api/ficheHoraires/json?route=${ligne}&time=${temp}`,
    success: function(json) {

        $('#liste1').empty()
        $('#liste2').empty()
         prevTime = json[direction].prevTime
         nextTime = json[direction].nextTime
         prefetch = ligne
        let terminus = json[0].arrets.length - 1



        $('#direction').html( ` --- Direction => ${json[direction].arrets[terminus].stopName} --- Cliquez pour changer de Direction`)

        layers1[ligne] = L.layerGroup();

    for (i=0 ; i < json[0].arrets.length ; i++){
        L.marker([json[0].arrets[i].lat, json[0].arrets[i].lon]).bindPopup(json[0].arrets[i].stopName).addTo(layers1[ligne])
    }

    mymap.addLayer(layers1[ligne]);

        for (i=0 ; i< json[direction].arrets.length; i++){

            jQuery('<li/>', 
            {
            id: i,
            "class": 'arretsLignes',
            })
            .text(json[direction].arrets[i].stopName).appendTo('#liste1');
        }

        for (i=0 ; i< json[direction].arrets.length; i++){
            let heurePassageA = json[direction].arrets[i].trips[2]
     

            var date = new Date(0);
            if (heurePassageA == '|'){ 
                console.log("error : pas d'horaires")

            }else{
            date.setSeconds(heurePassageA); 
            var timeString = date.toISOString().substr(11, 8); 
             } 
            jQuery('<li/>', 
            {
            id: "h"+i,
            "class": 'horairesLigne',
            })
            .text(  timeString  ).appendTo('#liste2');
        }
        fetch(`http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${ligne}`)
    .then((Response)=> Response.json())
    .then((json)=> {

        // liste = json.features[0].properties.ZONES_ARRET
        let jsonColor = json.features[0].properties.COULEUR
    
        let myStyle = {
            "color": `rgb(${jsonColor})`
        };

        L.geoJSON(json, {
            style: myStyle
        }).addTo(mymap);
      
        console.log('apres tracage', json)

    })
 }});
 


});//Document Ready Function END
-- INSERT INTO country
-- VALUES (1, "France", "PARIS" , 17000000);

-- INSERT INTO country
-- VALUES (NULL , "Allemagne","Berlin", 12000000);

--selectionne l'item 
-- SELECT country_name, populat
-- FROM country;
-- WHERE populat >= 50000

--enleve les doublons
-- SELECT DISTINCT country_name, populat
-- FROM country;



--CREER UNE TABLE
-- CREATE TABLE country (
--     id INT  NOT NULL AUTO_INCREMENT,
--     country_name VARCHAR(255) NOT NULL,
--     capitale VARCHAR(255) NOT NULL,
--     populat INT NOT NULL DEFAULT 0,
--     PRIMARY KEY(id)

-- );


--CREER UNE TABLE AVEC LIEN FK

-- CREATE TABLE ville (
--     id INT  NOT NULL AUTO_INCREMENT,
--     city_name VARCHAR(255),
--     country_id INT NOT NULL,
--     PRIMARY KEY(id)
-- CONSTRAINT FK_city_country FOREIGN KEY(country_id) REFERENCES country(id)

-- );


--INSERT FK;
--ALTER TABLE city
--CONSTRAINT FK_city_country FOREIGN KEY(country_id) REFERENCES country(id)

--SUPPRIMER FK;
--ALTER TABLE city
--DROP CONSTRAINT FK_city_country;


-- DROP TABLE country;

--ajouter une colonne
-- ALTER TABLE country 
-- ADD COLUMN Lang VARCHAR(255) NOT NULL 


--supprimer une colonne
-- ALTER TABLE country
-- DROP COLUMN Lang


--modifier une colonne  
-- ALTER TABLE country
-- MODIFY populat BIGINT NOT NULL DEFAULT 0;

--renomer la colonne
-- ALTER TABLE country
-- CHANGE populat country_population;


-- Update une ou plusieurs valeurs
-- UPDATE produit 
-- set libelle_produit='Produit2'
-- WHERE libelle1_produit <> "Produits1";


-- Supprimer une / plusieurs lignes
-- DELETE FROM ville
-- WHERE num_ville= 2;


--Select
--SELECT num_ville FROM ville
--WHErE libelle_ville ="Paris";

--Select + Tri
-- SELECT * 
-- FROM ville
-- ORDER BY libelle_ville ASC/DESC;
--Option--
-- WHERE libelle_ville LIKE 'P%' % => joker
-- OR libelle_ville LIKE '%g%' => joker
-- OR libelle_ville LIKE 'P___' => 1x _ = une lettre 


--Select
--SELECT num_ville FROM ville
--WHERE libelle_ville = "Paris";
--OR libelle_ville = "Marseille";
--AND libelle_ville = "Toulouse";

-- Option plus rapide 
--WHERE libelle_ville IN ('Paris','Lyon','Toulouse');











